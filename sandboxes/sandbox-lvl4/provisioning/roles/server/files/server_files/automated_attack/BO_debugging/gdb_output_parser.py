#!/usr/bin/pyth

# This script is just a parser of debug.sh output because joined "gdb commands" are not written into it.
# The aim of this file is just to provide an illustration of the commands flow. 


f = open("out.txt", "r")
lines = f.readlines()


for line in lines:
        if "PART1" in line:
                print("***Firstly, we try to get segmentation fault.***\n")
        elif "PART2" in line:
                print("***Now, we try to get necessary parameters and constuct payload***\n")
        elif "Reading symbols from ../stack" in line:
                print("(gdb) file ../stack")
                print(line)
        elif "/*Function with buffer overflow problem*/" in line:
                print("(gdb) list bof")
                print(line)
        elif "Dump of assembler code for function bof:" in line:
                print("(gdb) disas /m bof")
                print(line)
        elif "$1" in line:
                print()
                print("(gdb) p $ebp")
                print(line)
        elif "$2" in line:
                print("(gdb) p &buffer")
                print(line)
        else:
                print(line, end ="")