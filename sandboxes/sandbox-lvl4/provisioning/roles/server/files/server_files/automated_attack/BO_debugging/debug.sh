#!/bin/bash

# Get our program into segmentation fault by sending getBO.txt - output of python3 -c "print('A' * 110)".
# BUFFER_SIZE in bof function is 100
echo PART1
gdb -ex 'file ../stack'  -ex "list bof" -ex "disas /m bof" -ex "run < getBO.txt" -ex 'p $ebp' -ex 'quit'

echo PART2
# Get current values of ebp and buffer's address
gdb -ex 'file ../stack' -ex 'break bof' -ex 'run < noBO.txt' -ex 'p $ebp' -ex 'p &buffer' -ex 'quit'

