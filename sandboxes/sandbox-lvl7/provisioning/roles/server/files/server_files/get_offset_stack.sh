#!/bin/bash

echo '### DEBUGGING  ###'
sleep 2

echo '### OFFSET SEARCH of EIP register ###'
sleep 1

echo 
echo "### PATTERN_CREATE and PATTERN_SEARCH module of GDB-PEDA extention used"
sleep 3

gdb -ex 'file ./stack' -ex 'pattern_create 200 pattern' -ex 'run $(cat pattern)' -ex 'pattern_search' -ex 'quit'  | sudo tee out.txt

echo
echo 
a=`python parser.py`
echo " OFFSET: ${a}"


sudo rm out.txt





