import sys
import re

f = open("out.txt", "r")
lines = f.readlines()


offset = 0

def parser():
        for line in reversed(lines):
            if "EIP+0 found at offset:" in line:
                offset = line.split()[-1]  
                print(offset)


def main():
        parser()

if __name__ == "__main__":
        sys.exit(main()) 

