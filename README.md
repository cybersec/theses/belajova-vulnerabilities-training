# belajova-vulnerabilities-training

The content of this repository was created as a practical part of the [diploma thesis](https://is.muni.cz/auth/rozpis/tema?fakulta=1433;obdobi=8064;sorter=vedouci;balik=58;tema=380048;uplne_info=1) and demonstration of persistent software vulnerabilities, or vulnerable lab environments production. The repository contains files for building virtual lab environments to train vulnerability exploitation. The training focuses on  **Stack Buffer Overflow** vulnerability with **(reverse) shell** as a payload. There are prepared several levels of attack differing in their difficulty - details about differences and used methods are described in _writeup.md_. 

Besides prebuilt sandboxes (folder _sandboxes_) which can be run directly, there are also prepared input files for [_Cyber Sandbox Creator_ (CSC)](https://gitlab.ics.muni.cz/muni-kypo-csc/cyber-sandbox-creator/-/wikis/home) tool (folder _provisioning_dirs_ and _topology.yml/topology-without-attacker.yml_) to generate portable files for building virtual environment using Vagrant and Ansible. 

# Structure of repository

- **provisioning_dirs** - _provisioning_ directories for Ansible network setup automation (CSC's input files)
- **topology.yml** - the definition of required network topology  (CSC's input files)
- **topology-without-attacker.yml** - the definition of required network topology without attacker VM for levels where the attacker is local (See the table with levels overview in _writeup.md_)
- **sandboxes** - already prepared virtual environments (outputs of CSC)
- **writeup.md** - description of attack levels and their solutions

# Requirements

Individual prepared sandboxes (folder _sandboxes_) are executable with following versions of tools: 

- Virtualbox 6.1
- Vagrant 2.2.x
- Ansible 2.9-2.12 



To build up sandboxes, also CSC of following version is required: 

- [Cyber Sandbox Creator 2.0.1](https://gitlab.ics.muni.cz/muni-kypo-csc/cyber-sandbox-creator/-/wikis/Installation)

# Network Topology 

- network: 10.10.30.0/24
    - vulnerable server: 10.10.30.5
    - attacker: 10.10.30.10
        - username: root
        - password: toor

**Note:** In the case of attacks where an attacker is defined as _local_, only the vulnerable server is provided. 

# Instructions

**Note:** To disable attacker's pop-up window during Vagrant building process, `provider.gui = false` for _vm.provider_ in Vagrantfile is needed to be set. In pre-prepared sandboxes, it is done by default. 

## Instructions to directly launch prepared sandboxes

In folder _sandboxes_, there are prepared pre-built sandbox directories marked by the attack level (details about levels in _writeup.md_). 

1. Navigate to _sandbox_lvl_ directory with a proper level. Vagrantfile must be present.
2. Run `vagrant up` command.

## Instructions to build lab environments with Cyber Sandbox Creator

1. Install _Sandbox Creator_ on your machine following instructions in Gitlab repository, part [**_I want to generate sandboxes with CSC (for advanced users)_**](https://gitlab.ics.muni.cz/muni-kypo-csc/cyber-sandbox-creator/-/wikis/Installation#i-want-to-generate-sandboxes-with-csc-for-advanced-users).

2. To generate sandbox, run the command `create-sandbox --provisioning-dir PROVISIONING_DIR_PATH TOPOLOGY_FILE_NAME.yml` (anywhere). 

- _PROVISIONING_DIR_PATH_ specifies the path to the chosen _provisioning-lvl_ directory based on the attack level whose content will be copied to sandbox/provisioning. All directories are prepared in _provisioning_dirs_ folder.
- _TOPOLOGY_FILE_NAME.yml_ must be replaced by the path of _topology.yml_ or _topology-without-attacker.yml_ file.

**Note:** The file _topology-without-attacker.yml_ is prepared for levels where no attacker VM is needed i.e. the attacker is local. 

3. After running CSC, the _sandbox_ will be generated. To build virtual network, navigate to sandbox and run `vagrant up` command in folder with Vagrantfile. 

**Following commands can be also useful (must run from the directory containing the Vagrantfile):**

- `vagrant status` to list all virtual machines and their status
- `vagrant destroy -f` to destroy all virtual machines
- `vagrant destroy device_name -f`  to destroy the virtual machine device_name
- `vagrant ssh device_machine` connects to VM with `device_machine` using SSH 


## Licence 

This project is fully open-sourced, licensed under the [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](https://gitlab.fi.muni.cz/kypolab/theses/belajova-vulnerabilities-training/-/blob/master/LICENSE).

## Sources

For constructing individual sandboxes and the vulnerable server, source code provided by the [SEED Labs project](https://seedsecuritylabs.org) or, more precisely, its exercise [Buffer-Overflow Attack Lab (Server Version)](https://seedsecuritylabs.org/Labs_20.04/Software/Buffer_Overflow_Server/) was used and adjusted by our needs.





