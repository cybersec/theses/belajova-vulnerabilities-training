import sys
import re

f = open("out.txt", "r")
lines = f.readlines()


offset = 0

def parser():
        for line in reversed(lines):
                if "[RSP]" in line: 
                        offset = re.search(r'\d+', line).group() 
                        print(offset)

def main():
        parser()

if __name__ == "__main__":
        sys.exit(main()) 

