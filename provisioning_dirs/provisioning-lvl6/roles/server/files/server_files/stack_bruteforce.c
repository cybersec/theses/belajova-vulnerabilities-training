/* Stack Buffer Overflow vulnerable program: stack.c */
/* Vulnerable function: bof() */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/* Changing following size of the buffer in bof() 
 * will change the layout of the stack.
 * Suggested value: between 100 and 400  */
#ifndef BUF_SIZE
#define BUF_SIZE 100
#endif

// allowed size of the program's input 
#ifndef INPUT_SIZE
#define INPUT_SIZE 500
#endif

void dummy_function(char *str);

// hard coded jmp *esp function 
void jmpesp()
{
        __asm__("jmp *%esp");
}


int bof(char *str)
{
    char buffer[BUF_SIZE];

    // The following statement has a buffer overflow problem 
    strcpy(buffer, str);       

    return 1;
}

int main(int argc, char **argv)
{
    char str[INPUT_SIZE];
    FILE *badfile;

    badfile = fopen("badfile", "r"); 
    if (!badfile) {
       perror("Opening badfile"); exit(1);
    }

    int length = fread(str, sizeof(char), INPUT_SIZE, badfile);
    printf("Input size: %d\n", length);
    //dummy_function(str);
	bof(str);
    fprintf(stdout, "==== Returned Properly ====\n");
    return 1;
}

// This function is used to insert a stack frame of size 
// 1000 (approximately) between main's and bof's stack frames. 
// The function itself does not do anything. 
//void dummy_function(char *str)
//{
//    char dummy_buffer[1000];
//    memset(dummy_buffer, 0, 1000);
//    bof(str);
//}

