/* Stack Buffer Overflow vulnerable program: stack.c */
/* Vulnerable function: bof() */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/* Changing following size of the buffer in bof() 
 * will change the layout of the stack.
 * Suggested value: between 100 and 400  */
#ifndef BUF_SIZE
#define BUF_SIZE 100
#endif

// allowed size of the program's input 
#ifndef INPUT_SIZE
#define INPUT_SIZE 500
#endif

void dummy_function(char *str);

int bof(char *str)
{
    char buffer[BUF_SIZE]; 

/* Based on type of level/exercise, the value of RBP(64bit)/EBP(32bit) registers 
 * and buffer's address can be shown or not. 

#if __x86_64__

    unsigned long int *framep;
    asm("movq %%rbp, %0" : "=r" (framep));
	
#if SHOW_FP
    printf("Frame Pointer (rbp) inside bof():  0x%.16lx\n", (unsigned long) framep);
#endif

#if SHOW_BA
    printf("Buffer's address inside bof():     0x%.16lx\n", (unsigned long) &buffer);
#endif

#else
	
    unsigned int *framep;   
    asm("mov %%ebp, %0" : "=r" (framep));
	
#if SHOW_FP
    printf("Frame Pointer (ebp) inside bof():  0x%.8x\n", (unsigned) framep);
#endif
#if SHOW_BA
    printf("Buffer's address inside bof():     0x%.8x\n", (unsigned) &buffer);
#endif

#endif
*/

	 //	Output prining was omitted to simplify automation of debugging process (attack).
	
	 // The following statement has a buffer overflow problem.
    strcpy(buffer, str);       
    return 1;
}

int main(int argc, char **argv)
{
    char str[INPUT_SIZE];
	
	int length = fread(str, sizeof(char), INPUT_SIZE, stdin);
	printf("Input size: %d\n", length);
    dummy_function(str);
    fprintf(stdout, "==== Returned Properly ====\n");
    return 1;
}


/* This function inserts a stack frame of size 1000 (approximately) between main's and bof's stack frames. 
Note: This function is changed comparing to previous levels - slightly more randomized size of memory block 
needed to be added as debugging attack does not include communication with server.c - and 
generate_random_env() fce is not called */

void dummy_function(char *str)
{
	
	srand(time(0));
    int max_number = 1050;
    int min_number = 1000;
	int dummy_buffer_size = rand() % (max_number + 1 - min_number) + min_number;
	char dummy_buffer[dummy_buffer_size];
	memset(dummy_buffer, 0, dummy_buffer_size);
	
    //char dummy_buffer[1000];
    //memset(dummy_buffer, 0, 1000);
	
	bof(str);
}

